#' Extract sentences from books
#' 
#' Extract all sentences from a book.
#' @param fname the name of the file which contains the book
extractSentences <- function(fname) {
  ## read the file; the paragraphs are stored in 'content'
  p <- readRDS(fname)$content
  ## remove line breaks
  p <- stri_replace_all_regex(p, "\r\n,*", " ")
  ## remove empty elements
  p <- setdiff(p, "")
  ## split to sentences
  p <- stri_extract_all_boundaries(
    p, opts_brkiter = stri_opts_brkiter(type = "sentence"))
  ## return as a vector
  unlist(p)
}

#' Clean the sentences
#' 
#' Remove all sentences which all weird characters from the sentences
#' @param x a character vector of sentences
cleanSentences <- function(x) {
  ## trim spaces from the tails
  x <- stri_trim_both(x)
  ## remove non-alphanumeric characters from the start of the sentence
  x <- stri_replace_first_regex(x, "^[^[[:alnum:]]]+ *", "")
  ## keep sentences which 
  ## ...do not contain ., ? or ! in internal positions
  x <- x[stri_detect_regex(x, "(\\.|\\!|\\?)[^$]", negate = TRUE)]
  ## ...do contain ., ? or ! in final position
  x <- x[stri_detect_regex(x, "(\\.|\\!|\\?)$")]
  ## ...do contain space 
  x <- x[stri_detect_fixed(x, " ")]
  ## ...do contain lower case letters
  x <- x[stri_detect_regex(x, "[[:lower:]]")]
  ## ...do not contain an upper case letter right after a lower case letter
  x <- x[stri_detect_regex(x, "[[:lower:]][[:upper:]]", negate = TRUE)]
  ## ...and do not start with lowercase letter
  x <- x[stri_detect_regex(x, "^[[:lower:]]", negate = TRUE)]
  ## remove parentheses from external positions
  x <- stri_replace_all_regex(x, "^\\(|\\)$", "")
  ## correct commas and spaces
  x <- stri_replace_all_regex(x, ",([^ ])", ", $1")
  x <- stri_replace_all_regex(x, " +", " ")
  ## return
  x
}
  
#' Process the books and bind them together
#' 
#' @param ids a character vector; the IDs of the books 
#' @param files character vector; the files which contain the books
#' @return The function returns a data.table with the ID of the books and the 
#'   cleaned sentences.
processBooks <- function(ids, files) {
  proc <- function(i) {
    setTxtProgressBar(pb, i)
    data.table(
      id = ids[i],
      sentence = cleanSentences(
        extractSentences(files[i])
      )
    )
  }
  ## initiate progress bar
  pb <- txtProgressBar(min = 0, max = length(ids), style = 3)
  ## process books
  out <- lapply(seq_along(ids), proc)
  ## row-bind and return
  rbindlist(out)
}
